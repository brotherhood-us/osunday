<?php
namespace OSunday\core;

/**
 * CMB2 Options
 * @version 0.1.0
 */

abstract class Options {
    /**
     * Option key, and option page slug
     * @var string
     */
    private $key = '';


    /**
     * Page parent to insert on sub menu
     * @var string
     */ 
    private $parent = "";

    /**
     * Options page metabox id
     * @var string
     */
    private $metabox_id = '';

    /**
     * Options Page title
     * @var string
     */
    protected $title = '';

    /**
     * Options Page hook
     * @var string
     */
    protected $options_page = '';

    /**
     * Holds an instance of the object
     *
     * @var Instance of Object Class Options 
     **/
    private static $instance = null;

    /**
     * Wrapper class css of icon
     *
     * @var string class css
     */
    private $icon;

    /**
     * Wrapper position in menu admin
     *
     * @var int 
     */
    private $position;

    /**
     * Option View
     */
    private $view = null;

    static protected $instances = array();

    /**
     * Constructor
     * @since 0.1.0
     */
    private function __construct() {
        // Set our title
        $this->init();
        $this->hooks();
    }

    /**
     * Public getter method for retrieving protected/private variables
     * @since  0.1.0
     * @param  string  $field Field to retrieve
     * @return mixed          Field value or exception is thrown
     */
    public function __get( $field ) {
        // Allowed fields to retrieve
        if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
            return $this->{$field};
        }
        throw new \Exception( 'Invalid property: ' . $field );
    }

    /**
     * Método clone do tipo privado previne a clonagem dessa instância
     * da classe
     *
     * @return void
     **/
    private function __clone() {
    }

    /**
     * Método unserialize do tipo privado para prevenir a desserialização
     * da instância dessa classe.
     *
     * @return void
     **/
    private function __wakeup() {
    }

    /**
     * Tell if is a subpage
     * @since 0.1.0
     * @return boolean true if is subpage, false if is page
     */
    public function has_parent(){
        return !empty($this->get_parent());
    }

    /**
     * Set the slug of parent page if exist
     * @since 0.1.0
     * @param string $value slug parent page
     * @return Options Object Class
     */ 
    public function set_parent($value) {
        $this->parent = $value;
        return  $this;
    }

    /**
     * Set the value of Page Options ID
     * @since 0.1.0
     * @param string $value Option key
     * @return Options Object Class
     */
    public function set_key($value) {
        $this->key = $value;
        return $this;
    }

    /**
     * Set the path to view show in the admin area
     * @since 0.1.0
     * @param string $value path to view
     * @return Options Object Class
     */
    public function set_view($value) {
        $this->view = $value;
        return $this;
    }

    public function set_metabox_id($value) {
        $this->metabox_id = $value;
        return $this;
    }

    public function set_title($value) {
        $this->title = $value;
        return $this;
    }


    /**
     * Set class css icon
     * @since 0.1.0
     * @param string $value css class name
     * @return Object Class Options
     */
    public function set_icon($value) {
        $this->icon = $value;
        return $this;
    }

    /**
     * Set position in admin menu 
     * @since 0.1.0
     * @param int $value position on menu
     * @return Object Class Options
     */
    public function set_position($value) {
        $this->position = $value;
        return $this;
    }

    /**
     * Wrapper function around cmb_get_option
     * @since  0.1.0
     * @param  string  $key Options array key
     * @return mixed        Option value
     */
    public function get_option( $field = '' ) {
       return \get_field($field, 'option' );
    }

    public function get_key() {
        return $this->key;
    }

    public function get_view() {
        return $this->view;
    }

    public function get_metabox_id() {
        return $this->metabox_id;
    }

    public function get_title() {
        return $this->title;
    }

    /**
     * Return slug id of Parent page
     * @since 0.1.0
     * @return string Slug value of parent page
     */
    public function get_parent() {
        return $this->parent;
    }


    /**
     * Return css class name of icon
     * @since 0.1.0
     * @return string Css class name
     */
    public function get_icon() {
        return $this->icon;
    }

    /**
     * Return position on admin menu 
     * @since 0.1.0
     * @return int position in admin menu
     */
    public function get_position() {
        return $this->position;
    }

    /**
     * Retorna uma instância única de uma classe.
     * 
     * @static var Singleton $instance A instância única dessa classe.
     *
     * @return Singleton A Instância única.
     **/
    public static function get_instance() {
        $class = get_called_class();
        if (! array_key_exists($class, self::$instances)) {
            self::$instances[$class] = new $class();
        }
        return self::$instances[$class];
    }

    /**
     * Initiate our hooks
     * @since 0.1.0
     */
    protected function hooks() {
        /* add_action( 'admin_init', array( $this, 'init' )); */
        add_action( 'admin_init', array( $this, 'init_load' ));
        add_action( 'admin_menu', array( $this, 'add_options_page' ));
    }

    public function init_load() {
        $this->register_settings();
    }

    protected function register_settings() {
        register_setting( $this->get_key(), $this->get_key());
    }

    /**
     * Add menu options page
     * @since 0.1.0
     */
    public function add_options_page() {
        if($this->has_parent()) {
            $this->options_page = add_submenu_page($this->get_parent() , $this->get_title(), $this->get_title(), 'manage_options', $this->get_key(), array( $this, 'admin_page_display' ));
        } else {
            $this->options_page = add_menu_page( $this->get_title(), $this->get_title(), 'manage_options', $this->get_key(), array( $this, 'admin_page_display' ), $this->get_icon(), $this->get_position());
        }
    }

    /**
     * Admin page markup. Mostly handled by CMB2
     * @since  0.1.0
     */
    public function admin_page_display() {
        $default = array(
            'key'=>$this->get_key(),
            'metabox_id'=>$this->get_metabox_id()
        );

        $vars = $this->var_page();

        if(isset($vars) && count($vars) > 0) {
           $default = array_merge($default,$vars);
        }
        extract($default);

        include($this->get_view());
    }

    public function var_page() {
        return array(); 
    }

    /**
     * Register our setting to WP
     * @since  0.1.0
     */
    public function init() {

    }

    /**
     * Add the options metabox to the array of metaboxes
     * @since  0.1.0
     */
    public function add_fields() {

    }

    /**
     * Register settings notices for display
     *
     * @since  0.1.0
     * @param  int   $object_id Option key
     * @param  array $updated   Array of updated fields
     * @return void
     */
    public function settings_notices( $object_id, $updated ) {
        if ( $object_id !== $this->key || empty( $updated ) ) {
            return;
        }
        add_settings_error( $this->key.'-notices', '', __( 'Settings updated.', 'vinhasoft' ), 'updated' );
        settings_errors( $this->key.'-notices' );
    }
}
