<?php
namespace OSunday\core;

use WPException,
    JsonableException,
    BadMethodCallException,
WP_Query;

/**
 * Wordpress models
 **/
/**
 * Post Model
 *
 * Core post fields directly from WordPress:
 *   id             // ID of the post
 *   post_author    // ID of the post author
 *   post_date      // timestamp in local time
 *   post_date_gmt  // timestamp in gmt time
 *   post_content   // Full body of the post
 *   post_title     // title of the post
 *   post_excerpt   // excerpt field of the post, caption if attachment
 *   post_status    // post status: publish, new, pending, draft, auto-draft, future, private, inherit, trash
 *   comment_status // comment status: open, closed
 *   ping_status    // ping/trackback status
 *   post_password  // password of the post
 *   post_name      // post slug, string to use in the URL
 *   to_ping        // to ping ??
 *   pinged         // pinged ??
 *   post_modified  // timestamp in local time
 *   post_modified_gmt // timestatmp in gmt tim
 *   post_content_filtered // filtered content ??
 *   post_parent    // id of the parent post, if attachment, id of the post that uses this image
 *   guid           // global unique id of the post
 *   menu_order     // menu order
 *   post_type      // type of post: post, page, attachment, or custom string
 *   post_mime_type // mime type for attachment posts
 *   comment_count  // number of comments
 *   filter         // filter ??
 *
 * Special MTV fields:
 *   post_meta      // an array containing all of the meta for this post
 *   blogid         // id number of the blog this post lives on
 *   post_format    // the post_format for this post
 *   url            // attachments only, url of the original uploaded image or whatever
 *   thumb_url      // attachments only, url of the thumbnail image, if thumbnails are enabled
 *
 * Post object functions
 *   password_required()
 *     Whether post requires password and correct password has been provided.
 *   is_sticky()
 *     Check if post is sticky.
 *   post_class()
 *     Retrieve the classes for the post div as an array.
 *   permalink()
 *     permalink for this post, from WP get_permalink()
 *   categories()
 *     returns an array of categories that are associated with this post
 *   tags()
 *     returns an array of tags that are associated with this post
 *   featured_image()
 *     Returns a Post object representing the featured image
 *   attachments( $extra_query_args )
 *     Returns a PostCollection object representing attachments (gallery images)
 *   the_time( $format )
 *     Returns a formatted date string. Works like WordPress's 'the_time'.
 *   the_date( $format )
 *     Returns a formatted date string. Works like WordPress's 'the_date'.
 *   make_excerpt( $more_text )
 *     Returns a generated excerpt. Simliar to how WordPress makes excerpts in The Loop.
 **/
class Post extends WPModel {

    public function get_type() { 
        return "post"; 
    }

    public function initialize($args) {
        $this->cache_groups =  array(
            'posts', 'post_meta', 'post_ancestors', 'post_format_relationships');
    }

    public function __toString() {
        return $this->attributes['post_title'];
    }

    public function save() {
        $data = $this->attributes;

        if ( ! empty($data['blogid']) ) {
            $blogid =& $data['blogid'];
            unset( $data['blogid'] );
        } else { 
            $blogid = get_current_blog_id(); 
        }

        if ( ! empty($data['metadata']) ) {
            $meta =& $data['metadata'];
            unset( $data['metadata'] );
        }

        if ( isset($data['post_format']) ) {
            $post_format =& $data['post_format'];
            unset( $data['post_format'] );
        }

        if ( isset($data['post_type']) || empty($data['post_type']) ) {
            $data['post_type'] = $this->get_type();
        }

        if ( !empty( $this->id ) ) {
            $data['ID'] =& $this->id;
        }

        $postid = \wp_insert_post( $data, true );

        if ( is_wp_error( $postid ) ) {
            echo $this->id.'<br />';
            echo $postid->get_error_message();
            die(__("Error in the post", 'mtv'));
        } else if ( $postid == 0 ) {
            throw new JsonableException(__("Couldn't update the post", 'mtv'));
        }

        if ( ! empty( $meta ) ) {
            foreach ( $meta as $key => $val ) { 
                \update_post_meta( $postid, $key, $val );
            }
        }

        if ( isset($post_format) ) {
            $result = set_post_format( $postid, $post_format );

            if ( is_wp_error( $result ) ) {
                throw new WPException( $result );
            }
        }

        $this->id = $postid;

        $this->clear_cache(); 

        $this->fetch(); // We refresh the post in case any filters changed the content
    }

    public function fetch() {
        $model = get_post( $this->id );
        $this->reload($model);
    }

    public function parse( $postdata ) {
        # Use the parent parse
        $ret = parent::parse( $postdata );

        if(!isset($ret['post_type']) || empty($this->id)){
            $ret['post_type'] = $this->get_type();
        } else if($ret['post_type'] != $this->get_type() ){
            throw new \Exception(__("Post is not an ".$this->get_type(), 'osunday'));
        } 

        if(empty($postdata)) {
            $this->id = 0;
        }

        $ret['metadata'] = array();

        $meta_keys = get_post_custom_keys($this->id);

        if ( is_array( $meta_keys ) ) {
            foreach( $meta_keys as $key ) {
                $ret['metadata'][$key] = get_post_meta($this->id, $key, true);
            }
        }

        if ( $ret['post_type'] == 'post' ) {
            $ret['post_format'] = get_post_format($this->id);
        }

        return $ret;
    }

    public function is_sticky() {
        return is_sticky($this->id);
    }

    public function post_class( $class='' ) {
        return get_post_class($class, $this->id);
    }

    public function permalink() {
        if ( get_current_blog_id() !== $this->blogid ) {
            $permalink = get_permalink($this->id);
            return $permalink;
        } else {
            return get_permalink($this->id);
        }
    }

    public function categories() {
        return get_the_category($this->id);
    }

    public function tags() {
        return get_the_tags($this->id);
    }
/*
    public function featured_image() {
        if ( !empty( $this->post_meta['_thumbnail_id'] ) )
            return AttachmentCollection::get(array(
                'id' => $this->post_meta['_thumbnail_id'],
                'blogid' => $this->blogid
            ));
        else return null;
    }

    public function get_attachments() {
        return AttachmentCollection::for_post( $this->id );
    }

    # TODO: optimize with SQL
    public function clear_attachments() {
        foreach ( $this->get_attachments() as $attachment ) {
            $attachment->post_parent = null;
            $attachment->menu_order = null;
            $attachment->save();
        }
    }

    public function set_attachments( $attachments ) {
        $this->clear_attachments();
        $menu_order = 0;
        foreach ( $attachments as $attachment ) {
            $attachment->post_parent = $this->id;
            $attachment->menu_order = $menu_order;
            $attachment->save();
            $menu_order++;
        }
}*/

    public function the_time($format = null) {
        if ($format) {
            return mysql2date($format, $this->post_date);
        }
        else {
            return mysql2date(get_option('time_format'), $this->post_date);
        }
    }

    public function the_date($format = null) {
        if ($format) {
            return mysql2date($format, $this->post_date);
        }
        else {
            return mysql2date(get_option('date_format'), $this->post_date);
        }
    }

    public function the_content() {
        return str_replace(']]>', ']]&gt;', apply_filters('the_content', $this->post_content) );
    }

    public function make_excerpt($more_text = null) {
        // Author inserted a <!--more--> tag
        $parts = get_extended($this->post_content);
        if (!empty($parts['extended'])) {
            $ret = trim($parts['main']);
            // Conditionally add a read more link and
            // clean up punctuation and ellipsis at end of excerpt
            $wc_excerpt = str_word_count($ret);
            $wc_content = str_word_count($this->post_content);
            if ($wc_excerpt < $wc_content) {
                $ret = preg_replace('/([\.,;:!?\'"]{4,})$/', '...', $ret . '...');
                if (!empty($more_text))
                    $ret = $ret . ' <a href="'. $this->permalink .'" class="more-link">'. $more_text .'</a>';
            }
        }
        // Excerpt is empty, so generate one
        if (empty($parts['extended'])) {
            $text = strip_shortcodes( $this->post_content );
            $text = apply_filters('the_content', $text);
            $text = str_replace(']]>', ']]&gt;', $text);
            $text = strip_tags($text);
            $excerpt_length = apply_filters('excerpt_length', 55);
            $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]', $this);
            $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
            if ( count($words) > $excerpt_length ) {
                array_pop($words);
                $text = implode(' ', $words);
                $text = $text . $excerpt_more;
            } else {
                $text = implode(' ', $words);
            }
            $ret = apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
        }
        /* TODO: cache results of this function */
        return $ret;
    }

    public static function find($args = array()) {
        $query = new \WP_Query( $args );
        $models = array();
        // The Loop
        while ( $query->have_posts() ) {
            $query->the_post();
            $models[] = new static(\get_the_ID());
        }
        \wp_reset_postdata(); 

        return $models;
    }
}
