<?php
namespace OSunday\core;

/**
 * Model Class 
 * @version 0.1.0
 */

use \ArrayAccess,
    \Exception,
    \LogicException,
    \BadMethodCallException,
    \JsonableException,
    \NotImplementedException;

abstract class Model {
    // Where the model's data is stored
    protected $attributes           = array();

    // Default model data used when a new model is created
    protected $defaults             = array();
    // Attributes that are OK to send over the wire
    protected $json_fields          = array();
    // WP Object Cache groups this model is associated with
    protected $cache_groups         = array();

    // Is this model's data sync'd with the DB?
    protected $_synchronized = false;

    // Attributes that have been changed since the last save or fetch
    protected $_previous_attributes = array();

    public function __construct($id = 0, $args=array() ) {
        $this->initialize( $args );
        
        if ( empty($this->defaults) ) {
            $this->set( $args );
        } else {
            $this->set( array_merge($this->defaults, $args) );
        }
    }

    /**
     * Call initialize when the model is created
     **/
    public function initialize( $args ) {

    }

    /**
     * Populate this model from permanent storage
     **/
    public function fetch() {
        // get my attributes from the db
        // $from_db = new Object;
        // pass the results to reload
        // $this->reload( $from_db );
        throw new NotImplementedException();
    }

    /**
     * Process the raw data from permanent storage
     **/
    public function parse( $data ) {
        // Make sure we have an array and not an object
        if ( is_object($data) ) {
            return (array) $data;
        } else {
            return $data;
        }
    }

    /**
     * Update this model with data from permanent storage
     **/
    public function reload( &$data ) {
        // Parse raw data from the DB
        $tmp = $this->parse($data);
        // Reset any change tracking
        $this->_previous_attributes = array();
        $this->_synchronized = true;
        // Set the attributes
        $this->set( $tmp, true );
    }

   /**
     * Set a bunch of attributes at once
     **/
    public function set( $args, $fetching = false ) {
        $this->attributes = array_merge( $this->attributes, (array) $args );
    }

     /**
     * Returns an array of selected values
     * TODO: Return all values if no params are given
     **/
    public function values(/* $key [,$key [,$key ...] ] */) {
        $keys = func_get_args();
        $ret = array();
        foreach ( $keys as $key ) {
            $ret[] = $this->$key;
        }

        return $ret;
    }

    /**
     * Delete all the attributes in this model
     **/
    public function clear() {
        // TODO: update $this->_previous_attributes with removed items
        foreach ( func_get_args() as $arg ) {
            $this->_previous_attributes[$arg] = $this->attributes[$arg];
            unset( $this->attributes[$arg] );
        }
    }

    public function is_valid() {
        throw new NotImplementedException();
    }

    public function is_new() {
        throw new NotImplementedException();
    }

    /**
     * Validate the data in this model
     **/
    public function validate() {
        throw new NotImplementedException();
    }

    public function save() {
        throw new NotImplementedException();
    }

     /**
     * Returns an assoc array of this model's data to send over the wire
     **/
    public function to_json() {
        // decide which fields to send over the wire
        if ( empty( $this->json_fields ) ) {
            return $this->attributes;
        } else {
            $ret = array();
            foreach ( $this->json_fields as $k )
                $ret[$k] = $this->attributes[$k];
            return $ret;
        }
    }

    //Default of object

    public function __toString() {
        return get_called_class();
    }

    public function &__get($name) {
        if(array_key_exists( $name, $this->attributes )) {
            return $this->attributes[$name];
        }
        else { 
            $this->set( array( $name => "" ) );
            return $this->attributes[$name];
        }
    }

    public function __set( $name, $val ) {
        $this->set( array( $name => $val ) );
    }

    public function __unset( $name ) {
        $this->clear( $name );
    }

    public function __isset( $name ) {
        return isset($this->attributes[$name]);
    }

    //FINDERS

    public static function find($args = array()){
        return new static($args); 
    }

    public static function findAll() {
        return static::find();
    }

    public static function findByID($id) {
        return null;
    }

    public static function findBySlug($slug) {
        return null;
    }

    public static function findByTitle($name) {
        return null;
    }
}
