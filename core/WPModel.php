<?php
namespace OSunday\core;

use WPException,
    JsonableException,
    BadMethodCallException,
WP_Query;

/**
 * WPModel
 *
 * Core post fields directly from WordPress:
 *
 * Special fields:
 *   metadata      // an array containing all of the meta for this post
 *   blogid         // id number of the blog this post lives on
 *   url            // attachments only, url of the original uploaded image or whatever
 *   thumb_url      // attachments only, url of the thumbnail image, if thumbnails are enabled
 *
 **/
abstract class WPModel extends Model {
    protected $cache_groups =  array();
    protected $id           =  0;

    public function __construct($id = 0, $args=array()) {
        parent::__construct(); 
        $this->id = $id;

        if(!empty($this->id)) {
            //Carrega informações do banco já populando o Model
            $this->fetch();
        }

        if(empty($this->attributes['product_type']) || $this->attributes['product_type'] != $this->get_type()) {
            $this->attributes['product_type'] = $this->get_type();
        }
    }

    public function getCacheGroups() {
        return  $this->cache_groups;
    }

    public function getId() {
        return  $this->id;
    }

    public function get_type() { 
        return ""; 
    }

    public function clear_cache() {
        if(!empty($this->id)) {
            # Invalidate cached data for this Post
            foreach ( $this->cache_groups as $group ) {
                wp_cache_delete($this->id, $group);
            }
        }
    }

    public function parse( $data ) {
        # Use the parent parse
        $ret = parent::parse( $data );
        # gonna pick a case
        if ( !empty($ret['ID']) ) {
            $this->id = $ret['ID'];
            unset($ret['ID']);
        }

        # Take only the fields we need, put them in a temp array
        # TODO: current_blog may not be correct
        $ret['blogid'] = get_current_blog_id();

        return $ret;
    }

    public function permalink() {
        return "";
    }
}
