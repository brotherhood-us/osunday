<?php 
namespace OSunday\core;

class Configuration {
    private $path;

    public function __construct($path){
        $this->path = $path;
    }
    
    public function load($name){
            require_once($this->path.'/'.$name.'.php');
    }
}
?>
