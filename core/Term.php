<?php
namespace OSunday\core;

/**
 * CMB2 Options
 * @version 0.1.0
 */

abstract class Term implements ITerm{

    protected $term_id; 
    protected $name;
    protected $slug;
    protected $term_group;
    protected $term_taxonomy_id;
    protected $description;
    protected $parent;
    protected $count;
    protected $fields;

    public function __construct($id = null) {
        $this->__register_fields();
        $this->setTerm_id($id);

        //se o tiver algum ID ele irá popular o objeto com as informações que estão no banco,
        //caso constrário ele irá entender que você está criando um novo Term
        if(!($id == null || $id == 0)) {
            $term = get_term($id, static::getTaxonomy());

            if(!empty($term) && !is_wp_error($term)) {
                $this->load($term);
            }
        }
    }

    public static function getTaxonomy() {
        return '';
    }

    public function __register_fields() {
        return null; 
    }

    protected function registerField($key, $db_name) {
        $this->fields[$key]['name'] = $db_name;
        $this->fields[$key]['value'] = '';

        return $this; 
    }

    public function setField($key, $data) {
        if(array_key_exists($key, $this->fields)) { 
            $this->fields[$key]['value'] = $data;
        }

        return $this; 
    }

    public function getField($key) {
        if(array_key_exists($key, $this->fields)) { 
            return $this->fields[$key]['value'];
        }
    }  

    public function getField_dbname($key) {
        if(array_key_exists($key, $this->fields)) { 
            return $this->fields[$key]['name'];
        }
    } 

    public function getTerm_id() {
        return $this->term_id;
    }

    protected function setTerm_id($term_id){
        $this->term_id = $term_id;
        return $this;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
        return $this;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function setSlug($slug){
        $this->slug = $slug;
        return $this;
    }

    public function getTerm_group(){
        return $this->term_group;
    }

    public function setTerm_group($term_group){
        $this->term_group = $term_group;
        return $this;
    }

    public function getTerm_taxonomy_id(){
        return $this->term_taxonomy_id;
    }

    public function setTerm_taxonomy_id($term_taxonomy_id){
        $this->term_taxonomy_id = $term_taxonomy_id;
        return $this;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
        return $this;
    }

    public function getParent(){
        return $this->parent;
    }

    public function setParent($parent){
        $this->parent = $parent;
        return $this;
    }

    public function getCount(){
        return $this->count;
    }

    protected function setCount($count){
        $this->count = $count;
    }

    protected function load($term) {
        $this->setTerm_id($term->term_id);

        $this->setName($term->name);

        $this->setSlug($term->slug);

        $this->setTerm_group($term->term_group);

        $this->setTerm_taxonomy_id($term->term_taxonomy_id);

        $this->setDescription($term->description);

        $this->setParent($term->parent);

        $this->setCount($term->count);

        if(count($this->fields) > 0) {
            foreach($this->fields as $key=>$field) {
                $value = get_field($field['name'], static::getTaxonomy().'_'.$this->getTerm_id());
                $this->setField($key, $value); 
            }
        }
    }

    public function save() {
        $term_id = 0;
        if($this->getTerm_id() == null || $this->getTerm_id() == 0) {
            $term_id = $this->create();
        } else {
            $term_id = $this->update();
        }

        if(!empty($term_id) && !is_wp_error($term_id)) {
            $this->update_fields();
        }

        return $term_id;
    }

    public  function create() {
        $term_id = 0;

        $result = wp_insert_term(
            $this->getName(), // the term 
            static::getTaxonomy(), // the taxonomy
            array(
                'parent'=> $this->getParent(),
                'alias_of'=> $this->getTerm_group(),
                'description'=> $this->getDescription(),
                'parent'=> $this->getParent(),
                'slug'=> $this->getSlug()
            )
        );
        if(!is_wp_error($result)) {
            $term_id = $result['term_id'];
            $term_id = intval($term_id);
            $this->setTerm_id($term_id);

            return intval($term_id);
        } 

        return $result;
    }

    public function update() {
        $term_id = 0;


        $result = wp_update_term(
            $this->getTerm_id(), // the term 
            static::getTaxonomy(), // the taxonomy
            array(
                'name' => $this->getName(), 
                'parent'=> $this->getParent(),
                'alias_of'=> $this->getTerm_group(),
                'description'=> $this->getDescription(),
                'parent'=> $this->getParent(),
                'slug'=> $this->getSlug()
            )
        );

	if(!is_wp_error($result)) {
            $term_id = $result['term_id'];
            $term_id = intval($term_id);
            $this->setTerm_id($term_id);

            return intval($term_id);
        } 

         return $result;
    }

    protected function update_fields() {
        if(count($this->fields) > 0) {
            foreach($this->fields as $field) {
                update_field($field['name'], $field['value'], static::getTaxonomy().'_'.$this->getTerm_id()); 
            }
        }
    }

    //FINDERS
    public static function findAll() {
        return get_terms( array(
            'taxonomy' => static::getTaxonomy(),
            'hide_empty' => false
        ) );
    }

    public static function findByID($term_id) {
        return get_term( $term_id, static::getTaxonomy());
    }

    public static function findBySlug($slug) {
        return get_term_by( 'slug', $slug, static::getTaxonomy()); 
    }

    public static function findByName($name) {
        return get_term_by( 'name', $name, static::getTaxonomy());
    }

    public static function findByPost($post_id) {
        return wp_get_post_terms( $post_id, static::getTaxonomy()); 
    }
}
