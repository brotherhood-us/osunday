<?php
namespace OSunday\core;

/**
 * CMB2 Options
 * @version 0.1.0
 */

interface IOptions {

    /**
     * Register our setting to WP
     * @since  0.1.0
     */
    public function init();

    /**
     * Add the options metabox to the array of metaboxes
     * @since  0.1.0
     */
    public function add_fields(); 
}
