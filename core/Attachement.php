<?php
namespace OSunday\core;

class Attachment extends Post {
    public function parse( $postdata ) {
        # Use the parent parse
        $ret = parent::parse( $postdata );
        # If this isn't an attachment, we haven't found what we're looking for
        if ( $ret['post_type'] != "attachment" )
            throw new ModelParseException(__("Post is not an attachment", 'mtv'));
        # Add some special fields depending on the post type
        $ret['url'] = wp_get_attachment_url($ret['id']);
        $ret['thumb_url'] = wp_get_attachment_thumb_url($ret['id']);
        return $ret;
    }
}
