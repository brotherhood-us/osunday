<?php
namespace OSunday\Helper;

class VetorHelper {

    public static function recursive_search( $needle, $haystack ) {
        foreach( $haystack as $key => $value ) {
            $current_key = $key;
            if($needle === $value || ( is_array( $value ) && self::recursive_search( $needle, $value ) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

    public static function get_value($name, $array, $default = null) {
        if(!is_array($array) || empty($array)) return null;

       return  isset($array[$name]) && !empty($array[$name]) ? $array[$name] : $default;
    }
}
