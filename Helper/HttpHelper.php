<?php
namespace OSunday\Helper;

class HttpHelper {
     public static function is_avaliable($url, $timeout=3) {
        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($handle,  CURLOPT_TIMEOUT, $timeout);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        curl_close($handle); 

        return ($httpCode == 302 ||  $httpCode == 200);
     }
}

