<?php
namespace OSunday\Helper;

class ImageHelper {
    public static function base64ToImage( $data, $filename,$extensao, $path) {
        $data = str_replace('data:image/'.$extensao.';base64,', '', $data);
        $data= str_replace(' ', '+', $data);
        $image = base64_decode($data);

        $file = $path.$filename;

        $success = file_put_contents($file, $image);
        if($success) {
            return $file;
        }

        return null;
    }

}

