<?php
namespace OSunday\Helper;

class XmlHelper {

    public static function arrayToXml( $data, $service, $parent_name="") {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ) {
                    $key = $parent_name; //dealing with <0/>..<n/> issues
                    $subnode = $service;
                } else if(!empty($key) && ($key != "_")) {
                    $subnode = $service->addChild($key);
                } else {
                    $subnode = $service;
                }

                $subnode = self::arrayToXml($value, $subnode, $key);

            } else {
                $service->addChild("$key", htmlspecialchars("$value"));
            }
        }
        return $service;
    }

    public static function xmlToArray($xml) {
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        return $array;
    }
}

