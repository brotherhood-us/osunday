<?php
namespace OSunday\Registers;


interface ITable {
    public function create();
    public function destruct();
    public function getName();
    public function getPrefix();
    public function setPrefix($value);
    public function getCharset();
    public function setCharset($value);
}
