<?php
namespace OSunday\Registers;

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );



abstract class Table implements ITable {
    private $name;
    private $prefix;
    private $charset;

    function __construct ($charset, $prefix="") {
        $this->charset = $charset;
        $this->prefix = $prefix;
    }

    public function schema() { 
        return "";
    }

    public function create() {
        if(!empty($this->schema())) {
            dbDelta($this->schema());
            return true;
        }

        return false;
    }

    public function destruct() {
        global $wpdb;
        $table = $this->getTableName();
        $wpdb->query("DROP TABLE IF EXISTS $table");
        return true;
    }

    public function getTableName(){
        $table_name = $this->getPrefix().$this->getName();
        return $table_name;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($value) {
        $this->name = $value;
    }


    public function getPrefix() {
        return $this->prefix;
    }

    public function setPrefix($value) {
        return $this->prefix = $value;
    }

    public function getCharset() {
        return $this->charset;
    }

    public function setCharset($value) {
        $this->charset = $value;
    }

    public function __toString() {
        return $this->getName();
    }

}
