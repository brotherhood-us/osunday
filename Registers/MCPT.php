<?php
namespace OSunday\Registers;

class MCPT extends CPT{

	protected $sexo = '';
	
	public function __construct($sexo, $post_type_names) {
		$this->set_sexo($sexo);
		parent::__construct($post_type_names);
		
		$labels = $this->make_labels($this->singular,$this->plural,$this->sexo);
		
		$rewrite = array(
			'with_front'	=> false,	
			'pages'       	=> true,
			'feeds'       	=> true,
		);
		$this->options = array(
			'label'               => __( $this->singular, 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'query_var'           => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'	  => 'post',
			'menu_position'		  => 5
		);
	}
	
	public function set_sexo($value){
		$this->sexo = ((is_string($value) && in_array($value, array('M','F'))) ?  $value : 'M');
	}
	
	protected function dicionario($sexo){
		$dicionario = array(
					'M'=>array(
						'all'				=>	'Todos os ',
						'view'				=>	'Ver o ',
						'add'				=>	'Adicionar o ',
						'new'				=>	'Novo ',
						'edit'				=>	'Editar o ',
						'update'			=>	'Atualizar o ',
						'search'			=>	'Pesquisar ',
						'not-found'			=>	'Nenhum',
						'find'				=>	' encontrado ',
					),
					'F'=>array(
						'all'				=>	'Todas as ',
						'view'				=>	'Ver a ',
						'add'				=>	'Adicionar a ',
						'new'				=>	'Nova ',
						'edit'				=>	'Editar a ',
						'update'			=>	'Atualizar a ',
						'search'			=>	'Pesquisar ',
						'not-found'			=>	'Nenhuma',
						'find'				=>	' encontrada ',
					)
		);
		
		if(array_key_exists($sexo,$dicionario))
		return $dicionario[$sexo];
	}
	
	protected function make_labels($singular,$plural,$sexo){
		$dicionario = $this->dicionario($sexo);
		
		return array(
			'name'                => _x( $singular, 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( $singular, 'text_domain' ),
			'parent_item_colon'   => __( $singular.' Pai :', 'text_domain' ),
			'all_items'           => __( $dicionario['all'].$plural, 'text_domain' ),
			'view_item'           => __( $dicionario['view'].$singular, 'text_domain' ),
			'add_new_item'        => __( $dicionario['add'].$singular, 'text_domain' ),
			'add_new'             => __( $dicionario['new'].$singular, 'text_domain' ),
			'edit_item'           => __( $dicionario['edit'].$singular, 'text_domain' ),
			'update_item'         => __( $dicionario['update'].$singular, 'text_domain' ),
			'search_items'        => __( $dicionario['search'].$plural, 'text_domain' ),
			'not_found'           => __( $dicionario['not-found'].$singular.$dicionario['find'], 'text_domain' ),
			'not_found_in_trash'  => __( $dicionario['not-found'].$singular.$dicionario['find'].' na Lixeira', 'text_domain' ),
		);
	}
	
	protected function add_action_helper($pages,$callback){
		$post = $this->post_type_name;
		add_action('admin_head', function () use($pages,$callback,$post) {
			$screen = get_current_screen();
			if(($post != $screen->post_type) || (!is_string($pages) && !is_array($pages))) return;
			
			if(is_string($pages)) $pages = array($pages);
			if(in_array($screen->base ,$pages))	{
				$callback();
			}
		});
	}
	
	public function add_help($id,$title,$pages,$callback){
		$this->add_action_helper($pages, function () use($id,$title,$callback) {
			$args = array(
					'id'       => $id, //unique id for the tab
					'title'    => $title, //unique visible title for the tab
					'callback' => $callback,  //actual help text
				);
			$screen = get_current_screen();	  
			$screen->add_help_tab($args);
		});
	}
	
	public function add_helper_sidebar($pages,$conteudo){
		$this->add_action_helper($pages, function () use($conteudo) {
			$screen = get_current_screen();
			$screen->set_help_sidebar($conteudo);
		});
	}
}
